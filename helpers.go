package main

import (
	"context"
	"errors"
	"log"
	"net/http"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Authorize compares a given token with the database of registered tokens, and (if a user exists with that token) ensures that they can access the given resource.
func Authorize(token string, url string) (User, error) {
	tokenA := strings.Split(token, ":")
	var u User
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := userTable.FindOne(ctx, bson.M{"Username": tokenA[0], "Token": tokenA[1]}).Decode(&u)
	if err != nil {
		return User{}, err
	}

	// User is known to exist; perform actual access checks
	urlA := strings.Split(url, "/")[1:]
	if urlA[1] != u.Username && !u.Admin {
		return User{}, errors.New("User does not have permission to access the requested resource")
	}

	return u, nil
}

// FatalError takes an error, an http response writer, and an http status code.
// It then logs the error, writes the status code header, and writes an empty payload.
func FatalError(err error, w http.ResponseWriter, code int) {
	log.Println(err)
	w.WriteHeader(code)
	w.Write([]byte{})
}

// GetAdmin finds the site admin and returns a User instance representing them.
func GetAdmin() (User, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	var admin User
	err := userTable.FindOne(ctx, bson.M{"Admin": true}).Decode(&admin)
	if err != nil {
		return User{}, err
	}

	return admin, nil
}

// GetFiles searches for files with the given BSON query and returns a cursor which can iterate the found files.
func GetFiles(ctx context.Context, query bson.M) (*mongo.Cursor, error) {
	return filesTable.Find(ctx, query)
}

// GetFilesD operates similarly to GetFiles except query is of type bson.D.
func GetFilesD(ctx context.Context, query bson.D) (*mongo.Cursor, error) {
	return filesTable.Find(ctx, query)
}

// GetFileNames asks for the sender or recipient of the files being searched for and returns a JSON list of the names of those files.
func GetFileNames(direction string, relatedUsername string) ([]string, error) {
	var rOrS string
	switch direction {
	case "incoming":
		rOrS = "Recipient"
	default:
		rOrS = "Sender"
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	cursor, err := GetFiles(ctx, bson.M{rOrS: relatedUsername})
	if err != nil {
		return []string{}, err
	}

	var results []string
	for cursor.Next(ctx) {
		var elem File
		err := cursor.Decode(&elem)
		if err != nil {
			return []string{}, err
		}

		results = append(results, elem.Name)
	}

	if err := cursor.Err(); err != nil {
		return []string{}, err
	}

	cursor.Close(ctx)
	return results, nil
}
