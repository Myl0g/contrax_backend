package main

import (
	"bytes"
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"image/png"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/julienschmidt/httprouter"
	"github.com/pquerna/otp/totp"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var userTable *mongo.Collection
var filesTable *mongo.Collection

// Index writes a very simple welcome message when the root is GET'd.
func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "Hello!\n")
}

// Authenticate takes a given username, salt, and salted hash and returns an authentication token if the provided information matches what's in the database.
// 400 returned if the salt or hash was not properly encoded in base64; 404 if the requested user wasn't found in the database;
// 500 if the server encounters an error while registering a new user or inserting a new token; 403 if the user's salt and hash do not match the ones in the database;
// and 200 if the authentication succeeds and the token is in the payload.
func Authenticate(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("Authenticate: " + r.URL.String())

	authA := strings.Split(r.Header.Get("Authentication"), ":")
	salt, err := base64.StdEncoding.DecodeString(authA[1])
	if err != nil {
		FatalError(err, w, http.StatusBadRequest)
		return
	}
	hash, err := base64.StdEncoding.DecodeString(authA[2])
	if err != nil {
		FatalError(err, w, http.StatusBadRequest)
		return
	}
	passcode, err := base64.StdEncoding.DecodeString(authA[3])
	if err != nil {
		FatalError(err, w, http.StatusBadRequest)
		return
	}

	var u User
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	err = userTable.FindOne(ctx, bson.M{"Username": ps.ByName("username")}).Decode(&u)
	if err != nil {
		FatalError(err, w, http.StatusNotFound)
		return
	}

	if u.Hash == "" && u.Salt == "" {
		log.Println("Registering new user...")

		key, err := totp.Generate(totp.GenerateOpts{Issuer: "contrax", AccountName: u.Username})
		if err != nil {
			FatalError(err, w, http.StatusInternalServerError)
		}

		var buf bytes.Buffer
		img, err := key.Image(200, 200)
		if err != nil {
			FatalError(err, w, http.StatusInternalServerError)
		}
		png.Encode(&buf, img)

		var replacedDocument bson.M
		err = userTable.FindOneAndReplace(
			ctx,
			bson.M{"Username": ps.ByName("username")},
			bson.D{
				{Key: "Username", Value: ps.ByName("username")},
				{Key: "Salt", Value: string(salt)},
				{Key: "Hash", Value: string(hash)},
				{Key: "Token", Value: ""},
				{Key: "TotpKey", Value: key.Secret()},
				{Key: "Admin", Value: false},
			}).Decode(&replacedDocument)

		if err != nil {
			FatalError(err, w, http.StatusInternalServerError)
		}

		w.WriteHeader(http.StatusAccepted)
		w.Write(buf.Bytes())
		return
	} else if u.Hash != string(hash) || u.Salt != string(salt) || !totp.Validate(string(passcode), u.TotpKey) {
		FatalError(errors.New("authentication failed"), w, http.StatusForbidden)
		return
	}

	var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

	rand.Seed(time.Now().UnixNano())
	b := make([]rune, 30)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	token := string(b)

	_, err = userTable.UpdateOne(
		ctx,
		bson.M{"Username": ps.ByName("username")},
		bson.D{
			primitive.E{
				Key: "$set",
				Value: bson.D{
					primitive.E{
						Key:   "Token",
						Value: token,
					},
				},
			},
		},
	)

	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(token))
}

// AuthAdmin returns status code 200 if the client is a site admin; 403 otherwise. 404 is returned if the client does not exist in the database.
// Requires the Authorization header in the format username:token
func AuthAdmin(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("AuthAdmin: " + r.URL.String())

	user, err := Authorize(r.Header.Get("Authorization"), r.URL.String())
	if err != nil {
		FatalError(err, w, http.StatusNotFound)
	}

	if user.Admin {
		w.WriteHeader(http.StatusOK)
		return
	}

	w.WriteHeader(http.StatusForbidden)
}

// DownloadPDF grabs a PDF based on the filename and username provided, and sends the byte content back to the client.
// 403 returned if the client could not be authorized; 404 returned if the given file does not exist for the client;
// 500 if the server could not retrieve any files from the database; 200 if the file exists and is in the payload.
// Requires the Authorization header in the format username:token
func DownloadPDF(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("DownloadPDF: " + r.URL.String())

	_, err := Authorize(r.Header.Get("Authorization"), r.URL.String())
	if err != nil {
		FatalError(err, w, http.StatusForbidden)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	inboundCursor, inboundErr := GetFilesD(ctx, bson.D{
		primitive.E{
			Key: "Recipient",
			Value: bson.D{
				primitive.E{
					Key: "$in",
					Value: bson.A{
						ps.ByName("username"),
						"*",
					},
				},
			},
		}})
	outboundCursor, outboundErr := GetFiles(ctx, bson.M{"Sender": ps.ByName("username"), "Name": ps.ByName("filename")})

	if inboundErr != nil && outboundErr != nil {
		FatalError(inboundErr, w, http.StatusNotFound)
		return
	}

	for inboundCursor.Next(ctx) {
		var elem File
		err := inboundCursor.Decode(&elem)
		if err != nil {
			FatalError(err, w, http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(elem.Content)
		return
	}
	for outboundCursor.Next(ctx) {
		var elem File
		err := outboundCursor.Decode(&elem)
		if err != nil {
			FatalError(err, w, http.StatusInternalServerError)
			return
		}

		w.WriteHeader(http.StatusOK)
		w.Write(elem.Content)
		return
	}

	if err := inboundCursor.Err(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	if err := outboundCursor.Err(); err != nil {
		log.Println(err)
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}

	inboundCursor.Close(ctx)
	outboundCursor.Close(ctx)

	w.WriteHeader(http.StatusNotFound)
}

// UploadPDF takes a PDF as the body of the request and uploads it to the given filename for the given user.
// Returns 403 if the client couldn't be authorized; 500 if the server could not insert the PDF into the database or couldn't find the recipient;
// and 200 if the operation succeeds.
// Requires the Authorization header in the format username:token
func UploadPDF(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("UploadPDF: " + r.URL.String())

	user, err := Authorize(r.Header.Get("Authorization"), r.URL.String())
	if err != nil {
		FatalError(err, w, http.StatusForbidden)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}

	var recipient string
	if ps.ByName("username") == user.Username {
		admin, err := GetAdmin()
		if err != nil {
			FatalError(err, w, http.StatusInternalServerError)
			return
		}

		recipient = admin.Username
	} else {
		recipient = ps.ByName("username")
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err = filesTable.InsertOne(ctx, bson.D{
		{Key: "Content", Value: body},
		{Key: "Name", Value: ps.ByName("filename")},
		{Key: "Sender", Value: user.Username},
		{Key: "Recipient", Value: recipient},
	})

	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

// GetSalt looks up a user's salt based on their username, and sends it back to the client.
// Returns 404 if the client was not found in the database; 200 if the operation succeeds and the salt is in the payload.
func GetSalt(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("GetSalt: " + r.URL.String())

	var result User
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err := userTable.FindOne(ctx, bson.M{
		"Username": ps.ByName("username"),
	}).Decode(&result)
	if err != nil {
		FatalError(err, w, http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(result.Salt))
}

// GetIncomingFileNames sends to the client a list of the filenames of all files (marked incoming) currently under their username.
// Returns 403 if the client couldn't be authorized; 500 if the server could not query the database for incoming files;
// and 200 if the lookup was successful and the filenames are in the payload.
func GetIncomingFileNames(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("GetIncomingFileNames: " + r.URL.String())

	_, err := Authorize(r.Header.Get("Authorization"), r.URL.String())
	if err != nil {
		FatalError(err, w, http.StatusForbidden)
		return
	}

	resDirect, err := GetFileNames("incoming", ps.ByName("username"))
	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	resWildcard, err := GetFileNames("incoming", "*")
	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	res := append(resDirect, resWildcard...)
	jsonRes, err := json.Marshal(res)
	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonRes)
}

// GetOutgoingFileNames sends to the client a list of the filenames of all files (marked outgoing) currently under their username.
// Returns 403 if the client couldn't be authorized; 500 if the server could not query the database for incoming files;
// and 200 if the lookup was successful and the filenames are in the payload.
func GetOutgoingFileNames(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("GetOutgoingFileNames: " + r.URL.String())

	_, err := Authorize(r.Header.Get("Authorization"), r.URL.String())
	if err != nil {
		FatalError(err, w, http.StatusForbidden)
		return
	}

	res, err := GetFileNames("outgoing", ps.ByName("username"))
	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	jsonRes, err := json.Marshal(res)
	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(jsonRes)
}

// GetUsers finds all active users in the database and returns a JSON list of their usernames in the payload.
// Returns status code 500 if the server failed to look up usernames in the database and 200 if the request succeeded and the list is in the payload.
func GetUsers(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	corsHeaders(w, r)
	log.Println("GetUsers: " + r.URL.String())

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cursor, err := userTable.Find(ctx, bson.M{})
	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	var results []string
	for cursor.Next(ctx) {
		var elem User
		err := cursor.Decode(&elem)
		if err != nil {
			FatalError(err, w, http.StatusInternalServerError)
			return
		}

		results = append(results, elem.Username)
	}

	if err := cursor.Err(); err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	cursor.Close(ctx)
	bytes, err := json.Marshal(results)
	if err != nil {
		FatalError(err, w, http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
	w.Write(bytes)
}

func corsHeaders(w http.ResponseWriter, r *http.Request) {
	header := w.Header()
	header.Set("Access-Control-Allow-Methods", r.Header.Get("Allow"))
	header.Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, Authentication")
	header.Set("Access-Control-Allow-Origin", os.Getenv("ORIGIN"))
}

func main() {
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb+srv://golang:" + os.Getenv("MONGODB_PASSWORD") + "@" + os.Getenv("MONGODB_DOMAIN") + "/contrax?retryWrites=true&w=majority"))
	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	err = client.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}

	defer client.Disconnect(ctx)
	defer cancel()

	db := client.Database(os.Getenv("MONGODB_DATABASE"))
	userTable = db.Collection("users")
	filesTable = db.Collection("files")
	router := httprouter.New()

	router.GlobalOPTIONS = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		// Set CORS headers
		corsHeaders(w, r)
		// Adjust status code to 204
		w.WriteHeader(http.StatusNoContent)
	})

	router.GET("/", Index)
	router.GET("/users/", GetUsers)
	router.GET("/users/:username/account", Authenticate)
	router.GET("/users/:username/account/salt", GetSalt)
	router.GET("/users/:username/account/admin", AuthAdmin)
	router.GET("/users/:username/filenames/incoming", GetIncomingFileNames)
	router.GET("/users/:username/filenames/outgoing", GetOutgoingFileNames)
	router.GET("/users/:username/files/*filename", DownloadPDF)
	router.POST("/users/:username/files/*filename", UploadPDF)

	log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), router))
}
