package main

// User represents an entry in the users collection of a MongoDB database.
// Username can never be an empty string; the other 3 fields can be empty.
type User struct {
	Salt     string
	Username string
	Hash     string
	Token    string
	TotpKey  string
	Admin    bool
}

// File represents a file stored in the files collection of a MongoDB database.
// None of the fields can be blank.
type File struct {
	Content   []byte
	Name      string
	Sender    string
	Recipient string
}
